# simplest python logger

The most simplest python log wrap with built in formatting

Just import it, create logger and do the things, that you usually do with logger.

Usage:

     import log
     
     logger = log.logger('main','sample.txt')
     
     logger.debug('Message!')
     
This code creates file with prefix timestamp in name and put it into ./logs dir/. All logs strings will be have module name prefix ('main' in example). Have fun!